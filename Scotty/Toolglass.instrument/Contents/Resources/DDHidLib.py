from Foundation import *
import objc

try:
    from IIKit.InstrumentLoader import InstrumentLoader
    bundle_path = InstrumentLoader.currentBundle.bundlePath() + u"/Contents/Frameworks/DDHidLib.framework"
except:
    bundle_path = '../Frameworks/DDHidLib.framework'
print bundle_path
objc.loadBundle('DDHidLib', globals(), bundle_path=bundle_path)

del bundle_path

# >>> from DDHidLib import *
# >>> class MyMouseDelegate(NSObject):
# ...     @objc.signature('v@:@i')
# ...     def ddhidMouse_xChanged_(self, mouse, dx):
# ...         print 'xchange:', dx
# ...     @objc.signature('v@:@i')
# ...     def ddhidMouse_yChanged_(self, mouse, dy):
# ...         print 'ychange:', dy
# ...     @objc.signature('v@:@I')
# ...     def ddhidMouse_buttonDown_(self, mouse, button):
# ...         print "button down:", mouse, button
# ... 
# /System/Library/Frameworks/Python.framework/Versions/2.7/Extras/lib/python/PyObjC/objc/_functions.py:18: UserWarning: Usage objc.typedSelector instead of objc.signature
#   warnings.warn("Usage objc.typedSelector instead of objc.signature")
# >>> d = MyMouseDelegate.alloc().init()
# >>> m = DDHidMouse.allMice()[0]
# >>> m.setDelegate_(d)
# >>> m.startListening()
# >>> m.stopListening()
# >>> m.setListenInExclusiveMode_(True)
# >>> m.startListening()
# >>> m.stopListening()