def walkMenu(menu, depth=0):
    shortcuts = {}
    indent = u'   '
    #print indent*depth, menu.title()
    items = menu.itemArray()
    for item in items:
        if item.isHiddenOrHasHiddenAncestor() or not item.isEnabled():
            continue
        #print indent*depth, item.title(), item.keyEquivalent()
        if item.keyEquivalent():
            shortcuts[item.keyEquivalent()+str(item.keyEquivalentModifierMask())] = (item.keyEquivalentStringRepresentation(), item.title())
        if item.hasSubmenu():
            shortcuts.update(walkMenu(item.submenu(), depth+1))
    if depth == 0:
        for key, (shortcut, title) in sorted(shortcuts.items()):
            print shortcut, title
    return shortcuts

import objc
class NSMenuItem(objc.Category(NSMenuItem)):
    def keyEquivalentStringRepresentation(self):
        if not self.keyEquivalent():
            return u''
        keyEquivalent = self.keyEquivalent()
        keyRep = keyEquivalent.upper()
        mask = self.keyEquivalentModifierMask()
        needsShift = (mask & NSShiftKeyMask) or (keyRep == keyEquivalent and keyEquivalent.lower() != keyEquivalent.upper())
        maskRep = u''
        if mask & NSCommandKeyMask:
            maskRep = u'\u2318'
        if needsShift:
            maskRep = u'\u21e7' + maskRep
        if mask & NSAlternateKeyMask:
            maskRep = u'\u2325' + maskRep
        if mask & NSControlKeyMask:
            maskRep = u'\u2303' + maskRep
        return maskRep+keyRep